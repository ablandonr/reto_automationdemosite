package co.com.proyectobase.screenplay.questions;

import co.com.proyectobase.screenplay.userinterface.AutomationDemoSite;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;


public class NuevaPagina implements Question<String>{

	public static NuevaPagina Verificar() {
		return new NuevaPagina();
	}

	@Override
	public String answeredBy(Actor actor) {
		return Text.of(AutomationDemoSite.TEXTO_CONFIRMADO).viewedBy(actor).asString();
	}
}
