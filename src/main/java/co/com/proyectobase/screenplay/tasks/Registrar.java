package co.com.proyectobase.screenplay.tasks;

import java.util.List;

import org.openqa.selenium.WebElement;

import co.com.proyectobase.screenplay.interactions.BuscarEnListaLi;
import co.com.proyectobase.screenplay.model.InformacionUsuario;
import co.com.proyectobase.screenplay.userinterface.AutomationDemoSite;
import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.SelectFromOptions;


public class Registrar implements Task{


	private List<InformacionUsuario> info;
	public Registrar(List<InformacionUsuario> info) {
		
		this.info = info;
	}
	@Override
	public <T extends Actor> void performAs(T actor) {
		actor.attemptsTo(Enter.theValue(info.get(0).getFirstName()).into(AutomationDemoSite.AREA_DEL_NOMBRE));
		actor.attemptsTo(Enter.theValue(info.get(0).getLastName()).into(AutomationDemoSite.AREA_APELLIDO));
		actor.attemptsTo(Enter.theValue(info.get(0).getAddres()).into(AutomationDemoSite.AREA_DIRECCION));
		actor.attemptsTo(Enter.theValue(info.get(0).getEmailAdress()).into(AutomationDemoSite.AREA_CORREO));
		actor.attemptsTo(Enter.theValue(info.get(0).getTelefono()).into(AutomationDemoSite.AREA_TELEFONO));
		if (info.get(0).getGenero().equals("Mujer")) {
			actor.attemptsTo(Click.on(AutomationDemoSite.AREA_MUJER));
		} else {actor.attemptsTo(Click.on(AutomationDemoSite.AREA_HOMBRE));}
		actor.attemptsTo(Click.on(AutomationDemoSite.AREA_PASATIEMPO_CRICKET));
		actor.attemptsTo(Click.on(AutomationDemoSite.AREA_PASATIEMPO_PELI));
		actor.attemptsTo(Click.on(AutomationDemoSite.AREA_LENGUAJE));
		List<WebElement> Lista_idiomas = AutomationDemoSite.AREA_LISTA_LENGUAJE.resolveFor(actor).findElements(By.tagName("li"));
		BuscarEnListaLi.listar(info.get(0).getLanguage1(),Lista_idiomas);
		BuscarEnListaLi.listar(info.get(0).getLanguage2(),Lista_idiomas);
		BuscarEnListaLi.listar(info.get(0).getLanguage3(),Lista_idiomas);
		actor.attemptsTo(SelectFromOptions.byVisibleText(info.get(0).getHabilidad()).from(AutomationDemoSite.AREA_HABILIDADES));
		actor.attemptsTo(SelectFromOptions.byVisibleText(info.get(0).getPais()).from(AutomationDemoSite.AREA_PAIS));
		actor.attemptsTo(Click.on(AutomationDemoSite.AREA_PAIS2));
		actor.attemptsTo(Enter.theValue(info.get(0).getPais2()+"\n").into(AutomationDemoSite.AREA_PAIS3));
		actor.attemptsTo(SelectFromOptions.byVisibleText(info.get(0).getFecha()).from(AutomationDemoSite.AREA_FECHA));
		actor.attemptsTo(SelectFromOptions.byVisibleText(info.get(0).getMes()).from(AutomationDemoSite.AREA_MES));
		actor.attemptsTo(SelectFromOptions.byVisibleText(info.get(0).getDia()).from(AutomationDemoSite.AREA_DIA));
		actor.attemptsTo(Enter.theValue(info.get(0).getClave()).into(AutomationDemoSite.AREA_CLAVE1));
		actor.attemptsTo(Enter.theValue(info.get(0).getClave2()).into(AutomationDemoSite.AREA_CLAVE2));
		actor.attemptsTo(Click.on(AutomationDemoSite.BOTON_CONFIRMAR));
	}
	public static Registrar LosCamposCompletos(List<InformacionUsuario> info) {
		return Tasks.instrumented(Registrar.class, info);
	}

}
