package co.com.proyectobase.screenplay.model;

public class InformacionUsuario {
	
	private String firstName;
	private String lastName;
	private String addres;
	private String emailAddres;
	private String telefono;
	private String genero;
	private String hobbies1;
	private String hobbies2;
	private String language1;
	private String language2;
	private String language3;
	private String habilidad;
	private String pais;
	private String pais2;
	private String fecha;
	private String mes;
	private String dia;
	private String clave;
	private String clave2;
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getAddres() {
		return addres;
	}
	public void setAddres(String addres) {
		this.addres = addres;
	}
	public String getEmailAdress() {
		return emailAddres;
	}
	public void setEmailAdress(String emailAdress) {
		this.emailAddres = emailAdress;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public String getGenero() {
		return genero;
	}
	public void setGenero(String genero) {
		this.genero = genero;
	}
	public String getHobbies1() {
		return hobbies1;
	}
	public void setHobbies1(String hobbies1) {
		this.hobbies1 = hobbies1;
	}
	public String getHobbies2() {
		return hobbies2;
	}
	public void setHobbies2(String hobbies2) {
		this.hobbies2 = hobbies2;
	}
	public String getLanguage1() {
		return language1;
	}
	public void setLanguage1(String language1) {
		this.language1 = language1;
	}
	public String getLanguage2() {
		return language2;
	}
	public void setLanguage2(String language2) {
		this.language2 = language2;
	}
	public String getLanguage3() {
		return language3;
	}
	public void setLanguage3(String language3) {
		this.language3= language3;
	}
	public String getHabilidad() {
		return habilidad;
	}
	public void setHabilidad(String habilidad) {
		this.habilidad = habilidad;
	}
	public String getPais() {
		return pais;
	}
	public void setPais(String pais) {
		this.pais = pais;
	}
	public String getPais2() {
		return pais2;
	}
	public void setPais2(String pais2) {
		this.pais2 = pais2;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	public String getMes() {
		return mes;
	}
	public void setMes(String mes) {
		this.mes = mes;
	}
	public String getDia() {
		return dia;
	}
	public void setDia(String dia) {
		this.dia = dia;
	}
	public String getClave() {
		return clave;
	}
	public void setClave(String clave) {
		this.clave = clave;
	}
	public String getClave2() {
		return clave2;
	}
	public void setClave2(String clave2) {
		this.clave2 = clave2;
	}

}
