package co.com.proyectobase.screenplay.interactions;

import java.util.List;
import org.openqa.selenium.WebElement;
import net.serenitybdd.core.annotations.findby.By;

public class BuscarEnListaLi {
	
	public static void listar (String x, List<WebElement> list) {
		
		for( int i=0; i<list.size();i++) {
			if(list.get(i).findElement(By.tagName("a")).getText().contains(x)) {
				list.get(i).click();
				break;
			}
		}
	}

}
