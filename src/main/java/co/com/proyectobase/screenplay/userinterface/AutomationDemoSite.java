package co.com.proyectobase.screenplay.userinterface;

import org.openqa.selenium.By;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("http://demo.automationtesting.in/Register.html")
public class AutomationDemoSite extends PageObject{
	
	public static final Target AREA_DEL_NOMBRE = Target.the("Ingresar el Nombre completo")
			.located(By.xpath("//*[@placeholder='First Name']"));
	public static final Target AREA_APELLIDO = Target.the("Ingresar el apellido completo")
			.located(By.xpath("//*[@placeholder='Last Name']"));
	public static final Target AREA_DIRECCION = Target.the("Ingresar la dirección")
			.located(By.xpath("//*[@ng-model='Adress']"));
	public static final Target AREA_CORREO = Target.the("Ingresar el correo electrónico")
			.located(By.xpath("//*[@ng-model='EmailAdress']"));
	public static final Target AREA_TELEFONO = Target.the("Ingrese el número de telefóno")
			.located(By.xpath("//*[@ng-model='Phone']"));
	public static final Target AREA_HOMBRE = Target.the("Ingresar si es hombre")
			.located(By.xpath("//*[@value='Male']"));
	public static final Target AREA_MUJER = Target.the("Ingresar si es mujer")
			.located(By.xpath("//*[@value='FeMale']"));
	public static final Target AREA_PASATIEMPO_CRICKET = Target.the("Ingresar una actividad")
			.located(By.xpath("//*[@value='Cricket']"));
	public static final Target AREA_PASATIEMPO_PELI = Target.the("Ingresar otra actividad")
			.located(By.xpath("//*[@value='Movies']"));
	public static final Target AREA_LENGUAJE = Target.the("Ingresar el/los lenguajes")
			.located(By.id("msdd"));
	public static final Target AREA_LISTA_LENGUAJE = Target.the("Lenguajes seleccionados")
			.located(By.xpath("//*[@class='ui-autocomplete ui-front ui-menu ui-widget ui-widget-content ui-corner-all']"));
	public static final Target AREA_HABILIDADES = Target.the("Selección de habilidades")
			.located(By.id("Skills"));
	public static final Target AREA_PAIS = Target.the("Selección de país")
			.located(By.id("countries"));
	public static final Target AREA_PAIS2 = Target.the("Seleccione otro país")
			.located(By.xpath("//*[@role='combobox']"));
	public static final Target AREA_PAIS3 = Target.the("Seleccione un país valido")
			.located(By.xpath("//*[@type='search']"));
	public static final Target AREA_FECHA = Target.the("Año de la fecha de nacimiento")
			.located(By.id("yearbox"));
	public static final Target AREA_MES = Target.the("Mes de nacimiento")
			.located(By.xpath("//*[@placeholder='Month']"));
	public static final Target AREA_DIA = Target.the("Dia de nacimiento")
			.located(By.xpath("//*[@placeholder='Day']"));
	public static final Target AREA_CLAVE1 = Target.the("Ingreso de contraseña")
			.located(By.id("firstpassword"));
	public static final Target AREA_CLAVE2 = Target.the("Ingreso de confirmación de contraseña")
			.located(By.id("secondpassword"));
	public static final Target BOTON_CONFIRMAR = Target.the("CLick en el botón para registrarse")
			.located(By.id("submitbtn"));
	public static final Target TEXTO_CONFIRMADO = Target.the("Texto de confirmación")
			.located(By.xpath("/html/body/section/div[1]/div/div[2]/h4[1]"));
}
