package co.com.proyectobase.screenplay.stepdefinitions;

import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static org.hamcrest.Matchers.equalTo;
import java.util.List;
import org.openqa.selenium.WebDriver;
import co.com.proyectobase.screenplay.model.InformacionUsuario;
import co.com.proyectobase.screenplay.questions.NuevaPagina;
import co.com.proyectobase.screenplay.tasks.Abrir;
import co.com.proyectobase.screenplay.tasks.Registrar;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.thucydides.core.annotations.Managed;

public class registrowebStepDefinition {
	
	@Managed(driver="chrome")
	private WebDriver navegadorChrome;
	private Actor carlos = Actor.named("Carlos");
	
	@Before
	public void configuracionInicial() {
		carlos.can(BrowseTheWeb.with(navegadorChrome));
	}
	
	@Given("^que Carlos quiere acceder a la Web Automation Demo Site$")
	public void queCarlosQuiereAccederALaWebAutomationDemoSite() throws Exception {
		carlos.wasAbleTo(Abrir.LapaginaAutomationDemoSite());
	}

	@When("^el realiza el registro en la pagina$")
	public void elRealizaElRegistroEnLaPagina(List<InformacionUsuario> datosUsuario) throws Exception {
		carlos.attemptsTo(Registrar.LosCamposCompletos(datosUsuario));
	}

	@Then("^Verifica que se ingresa a la pantalla con el texto (.*)$")
	public void verificaQueSeIngresaALaPantallaConElTextoDoubleClickOnEditIconToEDITTheTableRow(String esperado) throws Exception {
		carlos.should(seeThat(NuevaPagina.Verificar(), equalTo(esperado)));
	}
}
