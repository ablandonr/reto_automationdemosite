#Author: ablandonr@choucairtesting.com

@Regresion
Feature: Registro en la pagina WebAutomation Demo Site
  Acceder a la pagina y registrarse

  @Caso1
  Scenario Outline: RetoWeb
    Given que Carlos quiere acceder a la Web Automation Demo Site
    When el realiza el registro en la pagina
    |firstName|lastName|addres|emailAddres|telefono|genero|hobbies1|hobbies2|language1|language2|language3|habilidad|pais|pais2|fecha|mes|dia|clave|clave2|
    |<firstName>|<lastName>|<addres>|<emailAddres>|<telefono>|<genero>|<hobbies1>|<hobbies2>|<language1>|<language2>|<language3>|<habilidad>|<pais>|<pais2>|<fecha>|<mes>|<dia>|<clave>|<clave2>|
    Then Verifica que se ingresa a la pantalla con el texto - Double Click on Edit Icon to EDIT the Table Row. 

    Examples:
    |firstName|lastName|addres|emailAddres|telefono|genero|hobbies1|hobbies2|language1|language2|language3|habilidad|pais|pais2|fecha|mes|dia|clave|clave2|
     ##@externaldata@./src/test/resources/datadriven/Data.xlsx@Hoja1
|Alejandra|Blandon|Choucair|choucair@testing.com|3205639878|Hombre|Cricket|Movies|Arabic|Thai|Japanese|Java|Colombia|Australia|1985|January|1|Abc123456|Abc123456|
|Alejandra|Blandon|Choucair|choucair@testing.com|3205639878|Hombre|Cricket|Movies|Portuguese|Thai|Japanese|Java|Colombia|Australia|1985|January|1|Abc123456|Abc123456|
|Alejandra|Blandon|Choucair|choucair@testing.com|3205639878|Hombre|Cricket|Movies|Portuguese|Thai|Japanese|Java|Colombia|Australia|1985|January|1|Abc123456|Abc123456|
